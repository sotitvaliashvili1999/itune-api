var month = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];
function search() {
  const query = document.getElementById("search-input");
  const request = fetch(`https://itunes.apple.com/search?term=${query.value}`);
  const tracks = document.getElementById("tracks-list");
  loading(true);
  request
    .then(response => response.json())
    .then(result => {
      const trackList = result.results;
      let trackInfo = "";
      for (let i = 0; i < trackList.length; i++) {
        let date = new Date(trackList[i].releaseDate);
        trackInfo += `<tr>
          <td><img src ="${trackList[i].artworkUrl100}"></td>
          <td>${trackList[i].artistName}</td>
          <td>${trackList[i].trackName}</td>
          <td>${date.getDay()}/ ${
          month[date.getMonth()]
        }/${date.getFullYear()}</td>
          <td><audio controls><source src="${trackList[i].previewUrl}"/></td>
          </tr>`;
      }

      tracks.innerHTML = trackInfo;
    })
    .finally(() => loading(false));
}
function loading(state) {
  const loadingGif = document.getElementById("loading-gif");
  loadingGif.style.display = state ? "block" : "none";
}
